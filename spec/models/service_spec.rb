require 'rails_helper'

RSpec.describe Service, type: :model do
  let(:service) { FactoryGirl.create(:service) }

  describe 'associations' do
    it { is_expected.to have_many(:clients) }
    it { is_expected.to have_many(:messages).through(:clients) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:description) }
  end

  describe '.monetize' do
    it { is_expected.to monetize(:price) }
    it { is_expected.to monetize(:price).with_currency(:rub) }
  end
end
