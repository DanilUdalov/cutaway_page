require 'rails_helper'

RSpec.describe Message, type: :model do
  let(:s_1) { FactoryGirl.create(:s_1) }
  let(:s_2) { FactoryGirl.create(:s_2) }
  let(:message) { FactoryGirl.create(:message, service_ids: [s_1.id, s_2.id]) }

  describe 'associations' do
    it { is_expected.to have_many(:clients) }
    it { is_expected.to have_many(:services).through(:clients) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:full_name) }
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to validate_presence_of(:phone) }
  end
end
