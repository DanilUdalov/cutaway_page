require 'rails_helper'

RSpec.describe Client, type: :model do
  describe 'associations' do
    it { is_expected.to belong_to(:message) }
    it { is_expected.to belong_to(:service) }
  end
end
