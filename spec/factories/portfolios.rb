FactoryGirl.define do
  factory :portfolio do
    title Faker::Name.title
    description Faker::Lorem.sentence
    frame_code 'Do something'
  end
end
