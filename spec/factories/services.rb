FactoryGirl.define do
  factory :service do
    name Faker::Lorem.words
    price Faker::Number.decimal(2)
    description Faker::Lorem.sentence
  end
end
