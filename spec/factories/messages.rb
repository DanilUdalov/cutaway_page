FactoryGirl.define do
  factory :message do
    full_name Faker::Name.name_with_middle
    email Faker::Internet.email
    phone Faker::Lorem.words
    message Faker::Lorem.sentence
  end
end
