require 'rails_helper'

RSpec.describe LandingController, type: :controller do
  let!(:service) { create(:service) }
  let!(:portfolio) { create(:portfolio) }
  let!(:poster) { create(:poster) }

  subject { response }

  describe '#index' do

    before { get :index }

    it 'assigns @services' do
      expect(assigns(:services)).to eq([service])
    end

    it 'assigns @porthfolios' do
      expect(assigns(:porthfolios)).to eq([portfolio])
    end

    it 'assigns @posters' do
      expect(assigns(:posters)).to eq([poster])
    end

    it 'new @message' do
      expect(assigns(:message)).to be_a_new(Message)
    end
  end
end
