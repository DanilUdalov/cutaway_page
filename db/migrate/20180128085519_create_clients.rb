class CreateClients < ActiveRecord::Migration[5.1]
  def change
    create_table :clients do |t|
      t.belongs_to :service, foreign_key: true
      t.belongs_to :message, foreign_key: true
      t.timestamps
    end
  end
end
