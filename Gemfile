source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

gem 'rails', '~> 5.1.4'
gem 'puma', '~> 3.7'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.5'
gem 'jquery-rails', '~> 4.3', '>= 4.3.1'

gem 'slim-rails'
gem 'activeadmin'
gem 'money-rails', '~>1'
gem 'phony_rails'

gem 'materialize-sass'
gem 'material_icons'
gem 'font-awesome-rails'

gem 'will_paginate'
gem 'will_paginate-materialize'

gem 'ckeditor', github: 'galetahub/ckeditor'

gem 'pg', '~> 0.18'
gem 'rails_12factor', group: :production
gem 'sendgrid-ruby'

gem 'carrierwave'
gem 'cloudinary'
gem 'figaro'

gem 'sitemap_generator'

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'selenium-webdriver'
  gem 'rubocop'
  gem 'pry'
end

group :test do
  gem 'capybara', '~> 2.13'
  gem 'factory_girl_rails'
  gem 'faker'
  gem 'rails-controller-testing'
  gem 'rspec-rails'
  gem 'shoulda-matchers'
end

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
