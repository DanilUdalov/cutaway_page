Rails.application.routes.draw do
  root to: 'landing#index'
  resources :messages, only: [:new, :create]

  namespace :admin do
    mount Ckeditor::Engine => '/ckeditor'

    root to: 'sessions#new'

    resources :abouts, only: [:new, :create, :edit, :update]
    resources :achievements, except: [:show]
    resources :sessions, only: [:new, :create, :destroy]
    resources :services, except: [:show, :index]
    resources :portfolios, except: [:show]
    resources :posters, only: [:new, :create, :destroy]

    get '/dashboard' => 'dashboard#index'
    get 'logout', to: 'sessions#destroy'
    get 'login', to: 'sessions#new', as: 'login'
  end
end
