class Admin::PostersController < AdminsController
  before_action :poster_resource, only: [:destroy]

  def new
    @poster = Poster.new
  end

  def create
    @poster = Poster.create(image_params)
    if @poster.save
      redirect_to admin_dashboard_path
    else
      render :new
    end
  end

  def destroy
    @poster.destroy!
    redirect_back(fallback_location: admin_dashboard_path)
  end

  private

  def image_params
    params.require(:poster).permit(:image)
  end

  def poster_resource
    @poster = Poster.find(params[:id])
  end
end
