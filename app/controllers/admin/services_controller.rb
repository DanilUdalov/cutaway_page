class Admin::ServicesController < AdminsController
  before_action :service_resource, only: [:edit, :update, :destroy]

  def new
    @service = Service.new
  end

  def create
    @service = Service.create(service_params)
    if @service.save
      redirect_to admin_dashboard_path
    else
      render :new
    end
  end

  def update
    if @service.update(service_params)
      redirect_to admin_dashboard_path
    else
      render :edit
    end
  end

  def destroy
    @service.destroy!
    redirect_to admin_dashboard_path
  end

  private

  def service_params
    params.require(:service).permit(:name, :price, :description)
  end

  def service_resource
    @service = Service.find(params[:id])
  end
end
