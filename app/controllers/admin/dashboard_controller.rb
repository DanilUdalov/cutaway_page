class Admin::DashboardController < AdminsController
  def index
    @services = Service.order(:id)
    @achievements = Achievement.order(:id)
    @porthfolios = Portfolio.order(:id).paginate(page: params[:page], per_page: 10)
    @messages = Message.order(:id)
    @about = About.first
    @posters = Poster.order(:id)
  end
end
