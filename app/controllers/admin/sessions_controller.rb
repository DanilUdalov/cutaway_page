class Admin::SessionsController < AdminsController
  skip_before_action :require_admin, only: [:new, :create]

  def create
    if params[:email] == ENV['admin_email'] && params[:password] == ENV['admin_password']
      session[:admin] = true
      redirect_to admin_dashboard_path
    else
      render :new
    end
  end

  def destroy
    session[:admin] = false
    redirect_to admin_root_path
  end
end
