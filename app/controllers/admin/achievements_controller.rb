class Admin::AchievementsController < AdminsController
  before_action :achievement_resource, only: [:destroy]

  def new
    @achievement = Achievement.new
  end

  def create
    @achievement = Achievement.create(image_params)
    if @achievement.save
      redirect_to admin_dashboard_path
    else
      render :new
    end
  end

  def destroy
    @achievement.destroy!
    redirect_back(fallback_location: admin_dashboard_path)
  end

  private

  def image_params
    params.require(:achievement).permit(:url, :type)
  end

  def achievement_resource
    @achievement = Achievement.find(params[:id])
  end
end
