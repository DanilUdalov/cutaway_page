class Admin::AboutsController < AdminsController
  before_action :about_resource, only: [:edit, :update]

  def new
    @about = About.new
  end

  def create
    @about = About.create(about_params)
    if @about.save
      redirect_to admin_dashboard_path
    else
      render :new
    end
  end

  def update
    if @about.update(about_params)
      redirect_to admin_dashboard_path
    else
      render :edit
    end
  end

  private

  def about_params
    params.require(:about).permit(:body)
  end

  def about_resource
    @about = About.find(params[:id])
  end
end
