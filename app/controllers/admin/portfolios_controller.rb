class Admin::PortfoliosController < AdminsController
  before_action :project_resource, only: [:edit, :update, :destroy]

  def new
    @portfolio = Portfolio.new
  end

  def create
    @portfolio = Portfolio.create(projects_params)
    if @portfolio.save
      redirect_to admin_dashboard_path
    else
      render :new
    end
  end

  def update
    if @portfolio.update(projects_params)
      redirect_to admin_dashboard_path
    else
      render :edit
    end
  end

  def destroy
    @portfolio.destroy!
  end

  private

  def projects_params
    params.require(:portfolio).permit(:title, :description, :frame_code)
  end

  def project_resource
    @portfolio = Portfolio.find(params[:id])
  end
end
