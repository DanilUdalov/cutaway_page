class MessagesController < ApplicationController
  def create
    @message = Message.create(message_resource)
    
    if @message.save
      MessageMailer.new_message(@message).deliver
      MessageMailer.message_deliver(@message).deliver
      redirect_to root_path, notice: 'Ваше сообщение успешно отправлено. С вами свяжуться в ближайшее время'
    else
      render :new
    end
  end

  private

  def message_resource
    params.require(:message).permit(:full_name, :email, :phone, :message, service_ids: [])
  end
end
