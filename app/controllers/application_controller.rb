class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :meta_tags

  private

  def meta_tags
    $description = "Занимаемся услугами фото и видеосъёмок"
    $keywords = "NN VideoProd NNVideoProd Нижний Новгород
                 Дмитрий Романов дмитрий романов роман булычев
                 Роман Булычев NN Video Production"
  end
end
