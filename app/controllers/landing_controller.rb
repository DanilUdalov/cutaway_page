class LandingController < ApplicationController
  def index
    @about = About.first
    @achievements = Achievement.order(:type)
    @services = Service.order(:id)
    @porthfolios = Portfolio.order(:id).paginate(page: params[:page], per_page: 6)
    @message = Message.new
    @posters = Poster.order(:id)
  end
end
