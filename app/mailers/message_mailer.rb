class MessageMailer < ApplicationMailer
  default from: 'NN VideoProd <nn.videoprod@gmail.com>'
  default to: 'nnvideoprod@yandex.ru'

  def new_message(message)
    @message = message

    mail to: 'nnvideoprod@yandex.ru', subject: 'Пришло новое сообщение'
  end

  def message_deliver(message)
    @message = message
    mail(to: message.email, subject: 'Здравствуйте! Вы написали нам сообщение! Мы с вами свяжемся в ближайшее время')
  end
end
