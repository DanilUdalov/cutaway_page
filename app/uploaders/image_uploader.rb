class ImageUploader < CarrierWave::Uploader::Base
  include Cloudinary::CarrierWave

  def default_url
    [thumb, 'default_image.png'].compact.join('_')
  end

  process tags: ['photo_album_sample']
  process convert: 'jpg'

  version :small do
    eager
    resize_to_fit(50, 50)
    cloudinary_transformation quality: 80
  end

  version :thumb do
    eager
    resize_to_fit(200, 200)
    cloudinary_transformation quality: 80
  end

  version :medium do
    eager
    resize_to_fit(600, 400)
    cloudinary_transformation quality: 80
  end

  version :large do
    eager
    resize_to_fit(800, 600)
    cloudinary_transformation quality: 80
  end

  version :big_large do
    eager
    resize_to_fit(1200, 800)
    cloudinary_transformation quality: 80
  end
end
