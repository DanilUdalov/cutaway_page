jQuery(document).ready(function($){
  $("#about_us").click(function() {
    $('html, body').animate({
      scrollTop: $("#AboutUs").offset().top
    }, 1000);
  });
  $("#about_us_footer").click(function() {
    $('html, body').animate({
      scrollTop: $("#AboutUs").offset().top
    }, 1000);
  });
  $("#about_us_mobile").click(function() {
    $('html, body').animate({
      scrollTop: $("#AboutUs").offset().top
    }, 1000);
  });
  $("#service_list").click(function() {
    $('html, body').animate({
      scrollTop: $("#ServiceList").offset().top
    }, 1000);
  });
  $("#service_list_footer").click(function() {
    $('html, body').animate({
      scrollTop: $("#ServiceList").offset().top
    }, 1000);
  });
  $("#service_list_mobile").click(function() {
    $('html, body').animate({
      scrollTop: $("#ServiceList").offset().top
    }, 1000);
  });
  $("#portholio").click(function() {
    $('html, body').animate({
      scrollTop: $("#Portholio").offset().top
    }, 1000);
  });
  $("#portholio_footer").click(function() {
    $('html, body').animate({
      scrollTop: $("#Portholio").offset().top
    }, 1000);
  });
  $("#achievements").click(function() {
    $('html, body').animate({
      scrollTop: $("#Achievements").offset().top
    }, 1000);
  });
  $("#achievements_footer").click(function() {
    $('html, body').animate({
      scrollTop: $("#Achievements").offset().top
    }, 1000);
  });
  $("#portholio_mobile").click(function() {
    $('html, body').animate({
      scrollTop: $("#Portholio").offset().top
    }, 1000);
  });
  $("#contact").click(function() {
    $('html, body').animate({
      scrollTop: $("#Contact").offset().top
    }, 1000);
  });
  $("#contact_footer").click(function() {
    $('html, body').animate({
      scrollTop: $("#Contact").offset().top
    }, 1000);
  });
  $("#contact_mobile").click(function() {
    $('html, body').animate({
      scrollTop: $("#Contact").offset().top
    }, 1000);
  });
});
