jQuery(document).ready(function($){
  $(".button-collapse").sideNav();
  $('.carousel.carousel-slider').slider({full_width: true});
  $('.slider').slider({full_width: true});
  $('.modal').modal();
  $('.carousel').carousel();
  $('select').material_select();
});
