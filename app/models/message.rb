class Message < ApplicationRecord
  validates_presence_of :full_name
  validates_presence_of :email
  validates_presence_of :phone

  phony_normalize :phone

  has_many :clients
  has_many :services, through: :clients
end
