class Portfolio < ApplicationRecord
  validates_presence_of :title
  validates_presence_of :frame_code
  validates_presence_of :description
end
