class Service < ApplicationRecord
  validates_presence_of :name
  validates_presence_of :price
  validates_presence_of :description

  has_many :clients
  has_many :messages, through: :clients

  monetize :price_cents
end
